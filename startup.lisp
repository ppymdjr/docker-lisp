
(ql:quickload :swank)
(let ((swank::*loopback-interface* "0.0.0.0"))
  (swank:create-server :port 4015 :dont-close t))

(format t "Running...~%")
