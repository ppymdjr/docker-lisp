FROM debian:stretch
WORKDIR /app
ADD ./startup.lisp /app/startup.lisp
RUN cd /app; \
    apt-get -y update; \
    apt-get -y upgrade; \
    apt-get -y install wget; \
    wget --quiet --no-check-certificate https://ccl.clozure.com/ftp/pub/release/1.11/ccl-1.11-linuxx86.tar.gz; \
    mkdir -p /usr/local/src; \
    cd /usr/local/src; \
    tar xzf /app/ccl-1.11-linuxx86.tar.gz; \
    rm /app/ccl-1.11-linuxx86.tar.gz; \
    cd /app; wget https://beta.quicklisp.org/quicklisp.lisp; \
    /usr/local/src/ccl/lx86cl64 -l quicklisp.lisp -e "(quicklisp-quickstart:install)" -e '(let ((ql-util::*do-not-prompt* t)) (ql:add-to-init-file))' -e "(ql:quickload :swank)" -e "(ccl:quit)"
    
    
EXPOSE 4015
CMD /usr/local/src/ccl/lx86cl64 -l startup.lisp

